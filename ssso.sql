
INSERT INTO dbo.ord_Trn_Order
(
	Customer_Id, Order_Date, Promise_Date, Warehouse_Id, 
	Order_Type, Order_Amount, Paid_Amount, Freight_Charge, Handling_Charge, 
	Total_Amount, Order_Amount_Without_Discount, Discount_Pct, Tariff_Pct, Tariff_Amount, 
	Is_Tariff_Pct_Updated, Invoice_No, Paid_By_Id, Cheque_No, Cancel_Date, 
	Sales_Person_Code, Sales_Person_Code_2, Promo_Code, Client_PO_No, Reference_No, 
	Price_Quote_Id, Is_Online_Order, Order_Source, Order_Source_Id, Email_Id, 
	Address_1, Address_2, City, [State], Country, 
	Zip_Code, Contact_Person, Is_Same_Ship_Adddress, Ship_To_Id, Ship_To_Name,
	Carrier_Id, Ship_Address_1, Ship_Address_2, Ship_City, Ship_State, 
	Ship_Country, Ship_Zip_Code, Phone, Fax, Payment_Term_Code, 
	Term_Days, Term_COD, Created_By, Created_Date, Approved_By, 
	Approved_Date, Last_Modified_By, Last_Modified_Date, Is_Active, Is_Order_Deleted,
	Deleted_By, Deleted_Date
)
SELECT TOP 1
	DTBL.Customer_Id, Order_Date, Approved_Ship_Date, DTBL.Warehouse_Id, 
	2 AS Order_Type, HTBL.Grand_Total, 0 AS Paid_Amount, Freight_Charge, Handling_Charge, 
	Grand_Total, 0 AS  Order_Amount_Without_Discount, Order_Discount, Tariff_Pct, Tariff_Amount,
	Is_Tariff_Pct_Updated, 0 AS Invoice_No, 0 AS Paid_By_Id, '' AS Cheque_No, GETDATE(), 
	Sales_Person, Sales_Person_2, Promo_Code, Client_Ref_No, '' AS Reference_No,
	HTBL.Price_Quote_Id, 1 AS Is_Online_Order, 'ONLINE_ORDER', 0 AS Order_Source_Id, Communication_Email_Id, 
	Ship_Address, Ship_Address2, Ship_City, Ship_State, Ship_Country,
	Ship_Zipcode, Contact_Name, 1 AS Is_Same_Ship_Adddress, 0 AS Ship_To_Id, Ship_To, 
	0 AS Carrier_Id, Ship_Address, Ship_Address2, Ship_City, Ship_State, 
	Ship_Country, Ship_Zipcode, Contact_Phone, '' AS Fax, Payment_Term,
	1 AS Term_Days, '' AS Term_COD, HTBL.Order_Created_By, Order_Date,  0 AS Order_Approved_By, 
	Approved_Date, '' AS Last_Modified_By, HTBL.Last_Modified_Date, 1 AS Is_Active, 0 AS Is_Order_Deleted, 
	'' AS Deleted_By, '' AS Deleted_Date 

FROM	
	opp_Order_Htbl AS HTBL 
	LEFT OUTER JOIn opp_Order_Dtbl AS DTBL ON DTBL.Order_Id = HTBL.Order_Id 
WHERE
	Order_No = 103651



	select * from cus_Mst_Customer

	select * from ord_trn_order

	select * from ord_trn_order_detail


	--update ord_trn_order_detail set Is_Tariff_Eligible = 1 where BAS049M2JB


	SELECT
        DISTINCT OPP_Htbl.Order_Id AS Order_Id, ISNULL(OPP_Dtbl.Order_Dtbl_Id, 0) AS Order_Dtbl_Id,
		ISNULL(LTRIM(RTRIM(ord_log.PROD_CD)), 0) AS PROD_CODE, 
		CASE WHEN ord_log.PROD_CD LIKE 'SVC_%' THEN ord_log.UT_DESC 
		ELSE PC.Product_Description END AS PROD_DESCRIPTION,
		LTRIM(RTRIM(PC.Product_Description_2)) AS PROD_DESCRIPTION_2,
		LTRIM(RTRIM(PC.Product_Description_3)) AS PROD_DESCRIPTION_3,
		CAST(ISNULL(PC.Weight, 0)  * ISNULL(ord_log.ORDER_QTY, 0) AS DECIMAL(10,2)) AS Inventory_Unit_Weight,
        CAST(ISNULL(ord_log.ORDER_QTY, 0) AS DECIMAL(10,2)) AS QTY, CAST(ISNULL(ord_log.UNIT_PRS, 0) AS DECIMAL(18,2))AS DOLLAR,
		CAST(ISNULL(ord_log.DISCOUNT, 0) AS DECIMAL(10,2)) AS DISCOUNT,
		CAST(ISNULL(orders.HANDL_FEE, 0) AS DECIMAL(10,2)) AS Handling_Charge,
		CAST(ISNULL(orders.MISC_CHG, 0) AS DECIMAL(10,2)) AS Freight_Charge,
		CAST(ISNULL(orders.TAX_RATE,0) * 100 AS DECIMAL(10,2)) AS Tax_Pct,
		CAST(ISNULL(orders.INVS_TAX,0) AS DECIMAL(10,2)) AS Tax_Amount,
		ISNULL(ord_log.NT_NUM, 0) AS NT_NUM, ISNULL(OPP_Htbl.Tariff_Pct, 0) AS Tariff_Pct, ISNULL(OPP_Dtbl.Is_Tariff_Eligible, 0) AS Is_Tariff_Eligible,
		ISNULL(ord_log.WHS_NUM, '') AS WHS_NUM, 
		--CASE WHEN ISNULL(PC.Color_Code, '') = Domestic_Color.Color_Code THEN ISNULL(Color_Description, '') ELSE '' END Color_Description,
		--CASE WHEN ISNULL(PC.Color_Code, '') = Domestic_Color.Color_Code THEN 1 ELSE 0 END Is_Domestic_Color,
		CASE WHEN ISNULL(PC.Color_Code, '') = COLOR.Color_Code THEN ISNULL(COLOR.Color_Description, '') ELSE '' END Color_Description,
		CASE WHEN Is_Domestic_Stone = 1 AND Is_Active = 1 THEN 1 ELSE 0 END Is_Domestic_Color,
		ISNULL(OPP_Dtbl_Service.SI_Dtbl_Id,0) AS SI_Dtbl_Id,
		CASE WHEN ISNULL(OPP_Dtbl_Service.SI_Dtbl_Id,0) > 0 THEN
		 (
			SELECT COUNT(DTBL.SI_Dtbl_Id) FROM pqt_so_Service_Item_Dtbl DTBL
			INNER JOIN dbo.pqt_so_Service_Item_Htbl AS HTBL ON HTBL.SI_Htbl_Id = DTBL.SI_Htbl_Id
			 INNER JOIN omsdata.dbo.ord_log AS OL ON OL.PROD_CD = HTBL.Product_Code AND OL.ORD_NUM = ORDERS.ORD_NUM --AND OL.NT_NUM = ord_log.NT_NUM
			 WHERE DTBL.SI_Dtbl_Id = OPP_Dtbl_Service.SI_Dtbl_Id AND OL.ORD_NUM = ORDERS.ORD_NUM --AND HTBL.Product_Code = ord_log.PROD_CD 
		 ) ELSE -1 END AS Is_SI_Item_Removed, ord_log.COMM_LN,
		 CASE 
			WHEN (ISNULL(PCD.Available_Qty, 0) >= 0 AND ISNULL(PCD.In_Stock_Qty, 0) > 0) OR (ord_log.PROD_CD LIKE 'SVC_%') THEN ''
			WHEN ISNULL(PCD.Reordered_Qty, 0) > 0 THEN 'blue'
			ELSE 'red' END AS Order_Color
	FROM 
		omsdata.dbo.ord_log AS ord_log
		INNER JOIN dbo.Inventory_Product_Code AS PC ON PC.Product_Code = ord_log.PROD_CD
		INNER JOIN omsdata.dbo.orders AS orders ON orders.ORD_NUM = ord_log.ORD_NUM
		LEFT OUTER JOIN opp_Order_Htbl AS OPP_Htbl ON OPP_Htbl.Order_No = ord_log.ORD_NUM
		LEFT OUTER JOIN opp_Order_Dtbl AS OPP_Dtbl ON OPP_Dtbl.Order_Id = OPP_Htbl.Order_Id AND OPP_Dtbl.Product_Code = ord_log.PROD_CD AND OPP_Dtbl.Product_Code NOT LIKE 'SVC%'
		LEFT OUTER JOIN opp_Order_Dtbl AS OPP_Dtbl_Service ON OPP_Dtbl_Service.Order_Id = OPP_Htbl.Order_Id AND OPP_Dtbl_Service.Product_Code = ord_log.PROD_CD AND OPP_Dtbl_Service.Product_Description = ord_log.UT_DESC AND OPP_Dtbl_Service.Product_Code LIKE 'SVC%'
		--LEFT OUTER JOIN dbo.pqt_mst_Domestic_Color AS Domestic_Color ON Domestic_Color.Color_Code = PC.Color_Code
		LEFT OUTER JOIN dbo.Inventory_Mst_Product_Color AS COLOR ON COLOR.Color_Code = PC.Color_Code
		LEFT OUTER JOIN [dbo].[Inventory_Product_Code_Detail] AS PCD ON PCD.Product_Code = ord_log.PROD_CD 
					AND PCD.Warehouse_Id = OPP_Dtbl.Warehouse_Id
	WHERE 
		ORDERS.ORD_NUM = 103763 
	ORDER BY 
		ord_log.COMM_LN